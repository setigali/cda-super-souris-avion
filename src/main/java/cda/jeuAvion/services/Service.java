package cda.jeuAvion.services;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalTime;

import cda.jeuAvion.entite.Avion;

public class Service {
	public static void sauvegarde(Avion avion) {
		BufferedWriter bw = null;
		try {
			bw = new BufferedWriter(new FileWriter(System.getenv("TMP")+"\\SCORE",true));
			bw.write(avion.getNom()+";"+avion.getScore()+";"+LocalDate.now()+";"+LocalTime.now());
			bw.newLine();
			bw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			try {
				bw.close();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			e.printStackTrace();
		}
	}

	public static String chargerScore() {
		BufferedReader br = null;
		String resultat = "";
		try {
			br = new BufferedReader(new FileReader(System.getenv("TMP")+"\\SCORE"));
			while(br.ready()) {
				String[] a = br.readLine().split(";");
				resultat += a[0] + " " + a[1] + " " + a[2] + " " + a[3].split("\\.")[0] + "\n";
			}
		} catch(IOException e) {
			
		}
		return resultat;
	}
}
