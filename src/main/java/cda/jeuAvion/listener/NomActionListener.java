package cda.jeuAvion.listener;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import cda.jeuAvion.ihm.JeuFrame;

public class NomActionListener implements ActionListener {

	@Override
	public void actionPerformed(ActionEvent e) {
		if (JeuFrame.t.getText().matches("[\\w]{3,6}")) {
			JeuFrame.avion.setNom(JeuFrame.t.getText());
			JeuFrame.nom.setText("                   " + JeuFrame.avion.getNom());
			JeuFrame.f.setVisible(false);
			JeuFrame.nouvellePartie();
		}
		else {
			JOptionPane.showMessageDialog(null, "Nom incorrect");
			JeuFrame.t.setText("");
		}
	}

}
