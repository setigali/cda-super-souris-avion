package cda.jeuAvion.listener;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import cda.jeuAvion.ihm.JeuFrame;

public class EndActionListener implements ActionListener {

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == JeuFrame.bNewGame) {
			JeuFrame.frameNouvellePartie();
			JeuFrame.nouvellePartie();
			JeuFrame.end.setVisible(false);
		}
		else if(e.getSource() == JeuFrame.bScore) {
			JeuFrame.scoreTop20();
		}
		else if(e.getSource() == JeuFrame.bExit) {
			System.exit(0);
		}

	}

}
