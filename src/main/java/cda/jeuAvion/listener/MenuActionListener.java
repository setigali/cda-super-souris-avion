package cda.jeuAvion.listener;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import cda.jeuAvion.ihm.JeuFrame;

public class MenuActionListener implements ActionListener {

	@Override
	public void actionPerformed(ActionEvent e) {
		if ("Nouvelle Partie".equals(e.getActionCommand())) {
			JeuFrame.frameNouvellePartie();
		}
		else if("Quitter".equals(e.getActionCommand()))
			System.exit(0);
		else if("Top 20".equals(e.getActionCommand())) {
			JeuFrame.scoreTop20();
		}
		else if("Close".equals(e.getActionCommand()))
			JeuFrame.scoreTop.setVisible(false);

	}

}
