package cda.jeuAvion.listener;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.ImageIcon;

import cda.jeuAvion.ihm.JeuFrame;

public class AvionKeyListener implements KeyListener {

	@Override
	public void keyTyped(KeyEvent e) {

	}

	@Override
	public void keyPressed(KeyEvent e) {
		if (e.getExtendedKeyCode() == KeyEvent.VK_LEFT && JeuFrame.avion.getX() > 0) {
			JeuFrame.avion.setBg(new ImageIcon("images\\avionG.jpg").getImage());
			JeuFrame.avion.setBounds(JeuFrame.avion.getX() - 5, JeuFrame.avion.getY(), 50, 50);
		} else if (e.getExtendedKeyCode() == KeyEvent.VK_UP && JeuFrame.avion.getY() > 0) {
			JeuFrame.avion.setBounds(JeuFrame.avion.getX(), JeuFrame.avion.getY() - 5, 50, 50);
		} else if (e.getExtendedKeyCode() == KeyEvent.VK_RIGHT && JeuFrame.avion.getX() < 350) {
			JeuFrame.avion.setBg(new ImageIcon("images\\avionD.jpg").getImage());
			JeuFrame.avion.setBounds(JeuFrame.avion.getX() + 5, JeuFrame.avion.getY(), 50, 50);
		} else if (e.getExtendedKeyCode() == KeyEvent.VK_DOWN && JeuFrame.avion.getY() < 800) {
			JeuFrame.avion.setBounds(JeuFrame.avion.getX(), JeuFrame.avion.getY() + 5, 50, 50);
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		JeuFrame.avion.setBg(new ImageIcon("images\\avion.jpg").getImage());
	}

}
