package cda.jeuAvion.thread;

import java.util.concurrent.TimeUnit;

import javax.swing.ImageIcon;

import cda.jeuAvion.entite.Avion;
import cda.jeuAvion.entite.Meteorite;
import cda.jeuAvion.ihm.JeuFrame;

public class Impact extends Thread {
	public void run() {
		try {
			while (true) {
				if (impact(JeuFrame.m1, JeuFrame.avion)) {
					JeuFrame.avion.setPointDeVie(JeuFrame.avion.getPointDeVie() - JeuFrame.m1.getDegat());
					JeuFrame.m1.setToucher(true);
					JeuFrame.avion.setBg(new ImageIcon("images\\impact.jpg").getImage());
					try {
						TimeUnit.SECONDS.sleep(1);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					JeuFrame.avion.setBg(new ImageIcon("images\\avion.jpg").getImage());
				}
				if (impact(JeuFrame.m2, JeuFrame.avion)) {
					JeuFrame.avion.setPointDeVie(JeuFrame.avion.getPointDeVie() - JeuFrame.m2.getDegat());
					JeuFrame.m2.setToucher(true);
					JeuFrame.avion.setBg(new ImageIcon("images\\impact.jpg").getImage());
					try {
						TimeUnit.SECONDS.sleep(1);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					JeuFrame.avion.setBg(new ImageIcon("images\\avion.jpg").getImage());
				}
				if (impact(JeuFrame.m3, JeuFrame.avion)) {
					JeuFrame.avion.setPointDeVie(JeuFrame.avion.getPointDeVie() - JeuFrame.m3.getDegat());
					JeuFrame.m3.setToucher(true);
					JeuFrame.avion.setBg(new ImageIcon("images\\impact.jpg").getImage());
					try {
						TimeUnit.SECONDS.sleep(1);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					JeuFrame.avion.setBg(new ImageIcon("images\\avion.jpg").getImage());
	
				}
				if (impact(JeuFrame.m4, JeuFrame.avion)) {
					JeuFrame.avion.setPointDeVie(JeuFrame.avion.getPointDeVie() - JeuFrame.m4.getDegat());
					JeuFrame.m4.setToucher(true);
					JeuFrame.avion.setBg(new ImageIcon("images\\impact.jpg").getImage());
					try {
						TimeUnit.SECONDS.sleep(1);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					JeuFrame.avion.setBg(new ImageIcon("images\\avion.jpg").getImage());
					
				}
				JeuFrame.pv.setText("pv : " + JeuFrame.avion.getPointDeVie());
			}
		}catch(NullPointerException e) {
			
		}
	}

	private boolean impact(Meteorite m, Avion a) {
		if (m.isToucher())
			return false;
		if (m.getX() >= a.getX() && m.getX() <= (a.getX() + 50)) {
			if (m.getY() >= a.getY() && m.getY() <= (a.getY() + 50)) {
				return true;
			}
		}
		if (m.getX() + 50 >= a.getX() && m.getX() + 50 <= (a.getX() + 50)) {
			if (m.getY() + 50 >= a.getY() && m.getY() + 50 <= (a.getY() + 50)) {
				return true;
			}
		}
		return false;
	}
}
