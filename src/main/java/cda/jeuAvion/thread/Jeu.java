package cda.jeuAvion.thread;

import java.util.concurrent.TimeUnit;

import javax.swing.JOptionPane;

import cda.jeuAvion.ihm.JeuFrame;
import cda.jeuAvion.services.Service;

public class Jeu extends Thread {

	public void run() {
		while (JeuFrame.avion.getPointDeVie() > 0) {
			JeuFrame.score.setText("                     score : " + JeuFrame.avion.getScore());
			Thread t1 = new Thread(JeuFrame.m1);
			t1.start();
			Thread t2 = new Thread(JeuFrame.m2);
			t2.start();
			Thread t3 = new Thread(JeuFrame.m3);
			t3.start();
			Thread t4 = new Thread(JeuFrame.m4);
			t4.start();
			try {
				TimeUnit.SECONDS.sleep(4);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if(JeuFrame.m1.getY() > 898) {
				JeuFrame.m1 = JeuFrame.createMeteorite();
			}
			if(JeuFrame.m2.getY() > 898) {
				JeuFrame.m2 = JeuFrame.createMeteorite();
			}
			if(JeuFrame.m3.getY() > 898) {
				JeuFrame.m3 = JeuFrame.createMeteorite();
			}
			if(JeuFrame.m4.getY() > 898) {
				JeuFrame.m4 = JeuFrame.createMeteorite();
			}
			if(JeuFrame.avion.getScore() > 999) {
				JeuFrame.avion.setScore(999);
			}
		}
		JOptionPane.showMessageDialog(null, "Game Over\n score : " + JeuFrame.avion.getScore());
		Service.sauvegarde(JeuFrame.avion);
		JeuFrame.endFrame();

	}
}
