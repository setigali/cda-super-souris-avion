package cda.jeuAvion.entite;

import cda.jeuAvion.ihm.FondPanel;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter


public class Avion extends FondPanel {
	
	//Attributs
	private String nom;
	private int pointDeVie;
	private int vitesse;
	private int score;
	
	//Constructeurs
	public Avion() {
		super("images\\avion.jpg");
		this.nom = "";
		this.pointDeVie = 1;
		this.vitesse = 5;
		this.score = 0;
	}
	
}
