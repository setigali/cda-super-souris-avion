package cda.jeuAvion.entite;

import cda.jeuAvion.ihm.FondPanel;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter


public abstract class Meteorite extends FondPanel implements Runnable{
	//Attributs
	private boolean toucher;
	private int vitesse;
	private int degat;
	
	public Meteorite(String image) {
		super(image);
		this.toucher = false;
	}
}
