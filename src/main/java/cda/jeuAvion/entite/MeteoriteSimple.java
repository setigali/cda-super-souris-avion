package cda.jeuAvion.entite;

import java.util.Random;
import java.util.concurrent.TimeUnit;

import cda.jeuAvion.ihm.JeuFrame;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class MeteoriteSimple extends Meteorite {
	// Attributs

	public MeteoriteSimple() {
		super("images\\meteoritesimple.jpg");
		this.setVitesse(2);
		this.setDegat(1);
	}

	@Override
	public void run() {
		try {
			TimeUnit.SECONDS.sleep(new Random().nextInt(8) + 2);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.setToucher(false);
		int random = new Random().nextInt(350);
		this.setBounds(random, 0, 50, 50);
		for (int i = 0; i < 900; i++) {
			this.setBounds(random, i, 50, 50);
			try {
				TimeUnit.MILLISECONDS.sleep(this.getVitesse());
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if(this.getY() > 898 && !this.isToucher())
				JeuFrame.avion.setScore(JeuFrame.avion.getScore()+2);
		}

	}

}
