package cda.jeuAvion.ihm;

import java.awt.Graphics;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JLayeredPane;

public class FondPanel extends JLayeredPane {
	private Image bg;
	
	public FondPanel(String image) {
		this.bg = new ImageIcon(image).getImage();
	}
	
	public Image getBg() {
		return this.bg;
	}
	
	public void setBg(Image image) {
		this.bg = image;
	}
	
	public void paintComponent(Graphics g) {
		g.drawImage(bg, 0, 0, null);
	}
}
