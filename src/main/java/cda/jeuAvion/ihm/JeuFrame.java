package cda.jeuAvion.ihm;

import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.util.Random;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import cda.jeuAvion.entite.Avion;
import cda.jeuAvion.entite.Meteorite;
import cda.jeuAvion.entite.MeteoriteDeFeu;
import cda.jeuAvion.entite.MeteoriteDeGlace;
import cda.jeuAvion.entite.MeteoriteSimple;
import cda.jeuAvion.entite.MeteoriteZigZag;
import cda.jeuAvion.listener.AvionKeyListener;
import cda.jeuAvion.listener.EndActionListener;
import cda.jeuAvion.listener.MenuActionListener;
import cda.jeuAvion.listener.NomActionListener;
import cda.jeuAvion.services.Service;
import cda.jeuAvion.thread.Defilement;
import cda.jeuAvion.thread.Impact;
import cda.jeuAvion.thread.Jeu;

public class JeuFrame extends JFrame {
	public static JLayeredPane jeu;
	public static FondPanel fond;
	public JMenuBar menuBar;
	public JMenu menu;
	public JMenuItem nouvellePartie;
	public JMenuItem top20;
	public JMenuItem quitter;
	public static JLabel nom;
	public static JLabel score;
	public static JLabel pv;
	public static Avion avion;
	public static Meteorite m1;
	public static Meteorite m2;
	public static Meteorite m3;
	public static Meteorite m4;
	public static JTextField t;
	public static JFrame f;
	public static JFrame end;
	public static JButton bNewGame;
	public static JButton bScore;
	public static JButton bExit;
	public static JFrame scoreTop;
	public static JTextArea textScore;

	public JeuFrame() {
		
		//ajout menu
		menuBar = new JMenuBar();
		menu = new JMenu("Fichier");
		nouvellePartie = new JMenuItem("Nouvelle Partie");
		nouvellePartie.addActionListener(new MenuActionListener());
		top20 = new JMenuItem("Top 20");
		top20.addActionListener(new MenuActionListener());
		quitter = new JMenuItem("Quitter");
		quitter.addActionListener(new MenuActionListener());
		menuBar.add(menu);
		menu.add(nouvellePartie);
		menu.add(top20);
		menu.add(quitter);
		this.setJMenuBar(menuBar);
		
		jeu = new JLayeredPane();
		this.add(jeu);

		//ajout fond de page
		fond = new FondPanel("images\\espace.jpg");
		fond.setOpaque(true);
		jeu.add(fond, new Integer(0));
		
		//Creation de l'avion
		avion = new Avion();
		avion.addKeyListener(new AvionKeyListener());
		avion.setFocusable(true);
		
		//Ajout de l'avion
		jeu.add(avion, new Integer(1));

		//Creation des meteorites
		m1 = createMeteorite();
		m2 = createMeteorite();
		m3 = createMeteorite();
		m4 = createMeteorite();
		
		//Ajout des meteorites
		jeu.add(m1, new Integer(2));
		jeu.add(m2, new Integer(3));
		jeu.add(m3, new Integer(4));
		jeu.add(m4, new Integer(5));

		//Ajout du score et pv
		score = new JLabel("                     score : " + avion.getScore());
		pv = new JLabel("pv : 0");
		nom = new JLabel("                "+avion.getNom());
		menuBar.add(score);
		menuBar.add(nom);
		menuBar.add(Box.createGlue());
		menuBar.add(pv);
		
		new Defilement().start();

		//preparation du lancement
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setTitle("Avion");
		this.pack();
		this.setVisible(true);
		this.setResizable(false);
		this.setSize(400, 900);
		this.setLocation(700, 50);
	}
	
	public static void nouvellePartie() {
		avion.setPointDeVie(5);
		avion.setScore(0);
		avion.setBounds(175, 750, 50, 50);
		pv.setText("pv : "+avion.getPointDeVie());
		new Impact().start();
		new Jeu().start();		
	}
	
	public static Meteorite createMeteorite() {
		int r = new Random().nextInt(4);
		switch (r) {
		case 1:
			return new MeteoriteDeFeu();
		case 2:
			return new MeteoriteDeGlace();
		case 3:
			return new MeteoriteZigZag();
		default:
			return new MeteoriteSimple();
		}
	}
	
	public static void frameNouvellePartie() {
		f = new JFrame();
		JPanel p = new JPanel();
		t = new JTextField(6);
		JButton b = new JButton("valider");
		b.addActionListener(new NomActionListener());
		p.setLayout(new FlowLayout());
		p.add(new JLabel("Nom"));
		p.add(t);
		p.add(b);
		f.add(p);
		
		//preparation du lancement
		f.setDefaultCloseOperation(EXIT_ON_CLOSE);
		f.setTitle("Nouvelle partie");
		f.pack();
		f.setVisible(true);
		f.setResizable(false);
		f.setSize(200, 200);
		f.setLocation(800, 400);

		
	}
	
	public static void endFrame() {
		end = new JFrame();
		end.setLayout(new GridLayout(3,2));
		bNewGame = new JButton("Valider");
		bScore = new JButton("Valider");
		bExit = new JButton("Valider");
		bNewGame.addActionListener(new EndActionListener());
		bScore.addActionListener(new EndActionListener());
		bExit.addActionListener(new EndActionListener());
		JLabel lNewGame = new JLabel("Relancer une nouvelle Partie");
		JLabel lScore = new JLabel("Afficher les scores");
		JLabel lExit = new JLabel("Sortir du jeu");
		end.add(lNewGame);
		end.add(bNewGame);
		end.add(lScore);
		end.add(bScore);
		end.add(lExit);
		end.add(bExit);
		
		end.setDefaultCloseOperation(EXIT_ON_CLOSE);
		end.setTitle("Fin du jeu");
		end.pack();
		end.setVisible(true);
		end.setResizable(false);
		end.setSize(300, 300);
		end.setLocation(800, 400);
	}
	
	public static void scoreTop20() {
		scoreTop = new JFrame();
		scoreTop.setLayout(new GridLayout(2, 1));
		textScore = new JTextArea(20, 200);
		textScore.setEditable(false);
		textScore.setText(Service.chargerScore());
		
		scoreTop.add(textScore);
		JButton bReturn = new JButton("Close");
		bReturn.addActionListener(new MenuActionListener());
		scoreTop.add(bReturn);
		
		scoreTop.setDefaultCloseOperation(EXIT_ON_CLOSE);
		scoreTop.setTitle("Score");
		scoreTop.pack();
		scoreTop.setVisible(true);
		scoreTop.setResizable(false);
		scoreTop.setSize(300, 200);
		scoreTop.setLocation(800, 400);
	}
}
