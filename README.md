# CDA Super Souris Avion

Monome :
	- Dorne Julien

## Information

Jeu d'avion

### Objectif

Esquiver les meteorites sur le chemin, 5 points de vie.

il y a 5 type de météorites :
	- meteorite simple : de couleur grise, enleve 1 point de vie si touché.
	- meteorite de Feu : de couleur rouge, enleve 2 points de vie
	- meteorite de Glace : de couleur bleu, enleve 2 points de vie
	- meteorite Zig Zag : de couleur rouge, enleve 2 points de vie, se déplace en zig zag de gauche a droite
	- meteorite Iceberg : de couleur bleu, enleve 4 points de vie
	
lorsqu'une meteorite atteint le bas de la fenetre, le joueur gagne des points.

## Lancement du jeu

Il faut que le dossier d'image soit au meme endroit que le .jar executable
Un ficher SCORE est créer via la variable d'environement TMP

Pour lancer une partie, lancer le .jar, cliquer sur l'onglet fichier, puis nouvelle partie, il faudra entrer son nom (entre 3 et 6 caractere) et le jeu commence.

## Probleme restant

Lors de la defaite du joueur, quand il relance une partie, la partie demarre avant que le joueur ai le temps d'ecrire son nouveau nom.
Lors du jeu, les Thread ont quelque probleme, certaine meteorite devienne invisible, d'autre clignote(thread divisé).
oublie de changer les tailles des type de meteorites.